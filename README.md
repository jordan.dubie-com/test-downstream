Maybe a bug in gitlab CI/CD: the pipeline (https://gitlab.com/jordan.dubie-com/test-downstream/-/pipelines/1252198177) makes a job starts before its dependencies jobs under the following conditions:
 - dependencies are manual (jobs "foo" and "bar" in the example)
 - on of the dependency have "allow_failure:true" attribute ("foo" job)
 - dependent job have rules: when:always (job "foobar" in the example)

Workaround: by giving specific exit_codes to allow_failure, the problem does not occur:
```
allow_failure:
   exit_codes: 1
```